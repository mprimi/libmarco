# README #

### What is this repository for? ###

This repository contains scripts and snippets in a good enough state to be shared publicly.
For comments and questions, my contact information is available at http://www.mpri.me

### How do I get set up? ###

There are different micro-projects in this repository, generally look for some documentation in the header of each file, or for README in each project folder.
